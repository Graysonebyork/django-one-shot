from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.
@admin.register(TodoList)
class TodosAdmin(admin.ModelAdmin):
    list_display = ["name", "id"]

@admin.register(TodoItem)
class TodosAdmin(admin.ModelAdmin):
    item_display = ["task", "id"]
