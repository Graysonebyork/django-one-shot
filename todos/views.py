from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoTaskForm



def show_todolist(request):
    list_list = TodoList.objects.all()
    context = {
        "todolist_object": list_list,
        "tester": [1, 2, 3, 4]
        }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    detail_list = get_object_or_404(TodoList, id=id)
    context = {
        "detail_list": detail_list,
        "tester": [2, 3, 4, 5],
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance = post)
        if form.is_valid():
            todo_list = form.save()
        return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "post_object": post,
        "form": form
    }
    return render(request, "todos/update.html", context)

def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    #todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoTaskForm(request.POST)
        if form.is_valid():
            todo_task = form.save()
            return redirect("todo_list_detail", id=todo_task.list.id)
    else:
        form = TodoTaskForm()
    context = {
        "form": form,
    }

    return render(request, "todos/create_task.html", context)

def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoTaskForm(request.POST, instance = post)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoTaskForm()

    context = {
        "post_object": post,
        "form": form
    }

    return render(request, "todos/update_item.html", context)
